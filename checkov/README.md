 This job runs checkov on a directory. The results are provided as JUnit report which feeds into the tests view of GitLab

Variables:
`CHECKOV_DIR`: set the directory where checkov runs. default: working directory

```yaml
include:
  - project: 'makandra/gitlab-ci-templates'
    ref: main # or use one of the tags to get a stable release
    file: '/checkov/job.yml'

checkov:
  extends: .checkov
  variables:
    CHECKOV_DIR: modulepath
    # optional you can inject additional parameters to checkov
    #ADDITIONAL_PARAMETERS: "--download-external-modules true"
```

## More advanced example for terraform

```
include:
  - project: 'makandra/gitlab-ci-templates'
    ref: main # or use one of the tags to get a stable release
    file: '/checkov/job.yml'


checkov:
  extends: .checkov
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      changes:
        - $CHECKOV_DIR/**
  parallel:
    matrix:
      - CHECKOV_DIR:
        - module1
        - module2
        - module3
  variables:
    ADDITIONAL_PARAMETERS: "--download-external-modules true"
```
