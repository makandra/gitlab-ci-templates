This template can be used to run pre-commit in gitlab CI pipelines.
```
include:
  - project: 'makandra/gitlab-ci-templates'
    ref: main # or use one of the tags to get a stable release
    file: 'pre-commit/pre-commit-template.yml'
```
## Image
The Variable `PRE_COMMIT_IMAGE` is required in the job definition. Value has to be a valid image reference of an image that is able to run all your configured pre-commit hooks.

## Autofix

By default the variable `PRE_COMMIT_AUTO_FIX` is set to `1`. That means if there are fixable style violations these will be fixed in the CI pipeline. For this to work the project needs a [Project access token](https://docs.gitlab.com/ee/api/project_access_tokens.html) with permissions to push to the project. The token must be stored in the CI variable `PRE_COMMIT_ACCESS_TOKEN`. Of course you can also use a [Group access token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html).

## Example .pre-commit-config.yaml configuration for terraform


```yaml
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.2.0
    hooks:
      - id: check-added-large-files
      - id: check-merge-conflict
      - id: check-vcs-permalinks
      - id: end-of-file-fixer
      - id: trailing-whitespace
        args: [--markdown-linebreak-ext=md]
        exclude: CHANGELOG.md
      - id: check-yaml
      - id: check-merge-conflict
      - id: check-executables-have-shebangs
      - id: check-case-conflict
      - id: mixed-line-ending
        args: [--fix=lf]
      - id: detect-aws-credentials
        args: ["--allow-missing-credentials"]
      - id: detect-private-key
  - repo: https://github.com/antonbabenko/pre-commit-terraform
    rev: v1.74.1
    hooks:
      - id: terraform_fmt
      - id: terraform_tflint
      - id: terraform_docs
        args:
          - --hook-config=--path-to-file=README.md
          - --hook-config=--add-to-existing-file=true
          - --hook-config=--create-file-if-not-exist=true
```
