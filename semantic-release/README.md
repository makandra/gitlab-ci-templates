# semantic-release

This template introduces [semantic-release](https://semantic-release.gitbook.io/semantic-release/) for your projects default branch (usually `main` or `master`).

## Setup in your project

To use it in your project add the following to your CI configuration:

```yaml
include:
  - project: 'makandra/gitlab-ci-templates'
    ref: main # or use one of the tags to get a stable release
    file: '/semantic-release/job.yml'

stages:
  - release
```

### Credentials for the release job

You also have to create a [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html). The token requires access to the `api` scope. Copy the token and create a new CI/CD variable named `GITLAB_TOKEN`.

## Semantic release configuration

Your project needs a `package.json` which could look like this:

```json
{
  "name": "testrelease",
  "license": "MIT",
  "devDependencies": {
    "@semantic-release/gitlab": "^11.0.1",
    "git-cz": "^4.9.0",
    "semantic-release": "^20.1.0"
  },
  "private": true,
  "release": {
  "plugins": [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    "@semantic-release/gitlab",
    "@semantic-release/npm",
    {
      "npmPublish": false
    }
  ],
    "branches": [
      "main",
      "master"
    ]
  }
}
```

You should set a name for your project and set it in the `name` setting. You should also check for newer releases of the dependencies.

In this example `npmPublish` is set to false. If your project should actually release a `npm` package you should set this to true. Please mind that this requires further configuration for authentication to the npm package registry.

For more information about the `semantic-release` configuration see [semantic-release/usage/configuration](https://semantic-release.gitbook.io/semantic-release/usage/configuration).

When you're done with your configuration run `npm install --package-lock-only` to create a `package-lock.json` and commit both files to your repository.
