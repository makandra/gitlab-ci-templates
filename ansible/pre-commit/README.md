The pre-commit image created through this image can be referenced via "registry.gitlab.com/makandra/gitlab-ci-templates/pre-commit-ansible:IMAGE_VERSION".

It can be used with the [pre-commit-template.yml](../../pre-commit/pre-commit-template.yml) to let `pre-commit` run in a gitlab CI pipeline for ansible projects.
