Templates for building docker images with kaniko.

If your CI environment requires using a proxy, set environment variables for
your pipelines. They will be passed on to kaniko as `--build-arg`s.

- `http_proxy`
- `https_proxy`
- `no_proxy`

## simple build

Variables:

- `DESTINATION`: The repository identifier of the final image. Default: `$CI_REGISTRY_IMAGE:$CI_DEFAULT_BRANCH`
- `CONTEXT`: The directory where the context and the Dockerfile for building the image resides. Defaults to the root of the repository.
- `DOCKERFILE`: The path to the Dockerfile. Default: `$CONTEXT/Dockerfile`
- `KANIKO_ARGS`: Additional arguments for executor. See: [kaniko/readme](https://github.com/GoogleContainerTools/kaniko/blob/master/README.md#additional-flags)

Caching is enabled so kaniko will push the layers to the repository of `DESTINATION` to speed up subsequent builds.

```yaml
include:
  - remote: https://gitlab.com/makandra/gitlab-ci-templates/-/raw/main/kaniko/build.yml

stages:
  - build
 ```

## build and push to ECR

Build and push an image to AWS ECR. This requires your GitLab to be connected to IAM to perform OAuth. The Kaniko Executor will assume the role definede in `AWS_ROLE_ARN` to authenticate for access to ECR.

The layer cache is pushed to the gitlab container repository, since pushing to ECR is not working out of the box. Therefore `--cache-repo $CI_REGISTRY_IMAGE` is set for `/kaniko/executor`

Variables:

- `CONTEXT`: The directory where the context and the Dockerfile for building the image resides. Defaults to the root of the repository.
- `DESTINATION`: The repository identifier of the final image.
- `AWS_ACCOUNT_ID`: AWS API authentication variable
- `AWS_REGION`: AWS API authentication variable
- `AWS_ROLE_ARN`: AWS API authentication variable
- `KANIKO_ARGS`: Additional arguments for executor. See: [kaniko/readme](https://github.com/GoogleContainerTools/kaniko/blob/master/README.md#additional-flags)

```yaml
include:
  - remote: https://gitlab.com/makandra/gitlab-ci-templates/-/raw/main/kaniko/ecr.yml

stages:
  - build

variables:
  CONTEXT: context
  DESTINATION: ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com/my-container:latest
  AWS_ACCOUNT_ID: <account id>
  AWS_REGION: <aws region>
  AWS_ROLE_ARN: arn:aws:iam::${AWS_ACCOUNT_ID}:role/<IAM role to assume>
 ```
