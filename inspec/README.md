Run inspec on INSPEC_DIR. The results are provided as JUnit report which feeds into the tests view of GitLab

Variables:
  `INSPEC_DIR`: path to the inspec tests ; default: ./inspec

```yaml
include:
  - project: 'makandra/gitlab-ci-templates'
    ref: main # or use one of the tags to get a stable release
    file: '/inspec/job.yml'

inspec:
  extends: .inspec
  variables:
    INSPEC_DIR: ./inspec
```
