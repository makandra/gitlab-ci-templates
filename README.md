This repository contains CI templates which makandra uses on multiple projects. This way all customers can benefit from the development of these templates.

For a detailed description of the CI jobs refer the `README.md` files in the job subdirectories.

# Contributing

Feel free to open a Merge Request with changes. Be sure to provide a description telling us why you're introducing the changes. Please provide commit messages in the style of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).
