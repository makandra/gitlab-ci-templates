This job runs tfSec on a directory. The results are provided as JUnit report which feeds into the tests view of GitLab

Variables:
  `TFSEC_DIR`: set the directory where tfsec runs. default: working directory

```yaml
include:
  - project: 'makandra/gitlab-ci-templates'
    ref: main # or use one of the tags to get a stable release
    file: '/terraform/tfsec/job.yml'

tfsec:
  extends: .tfsec
  variables:
    TFSEC_DIR: terraform
```
